﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOverMessageController : MonoBehaviour {
	string gameoverMessage = "GAME OVER\nyour record : ";
	calculateRunDistance runDistance;
	// Use this for initialization
	void Start () {
//		GameObject tmpObj = GameObject.Find ("RunDistance");
//		runDistance = tmpObj.GetComponent<calculateRunDistance>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void viewGameoverMessage(){
		Debug.Log ("viewGameoverMessage");
		runDistance = (GameObject.Find ("RunDistance")).GetComponent<calculateRunDistance>();
		gameoverMessage += ((int)(Mathf.Ceil(runDistance.GetRunDistance ()))).ToString() + " m";
		GetComponent<Text> ().text = gameoverMessage;

	}
}
