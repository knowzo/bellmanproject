﻿using UnityEngine;
using System.Collections;

public class BellPangController : MonoBehaviour {

	ParticleSystem particle=null;
	GameObject target;
	// Use this for initialization
	bool isPlaying=false;
	void Start () {
//		particle = GetComponent<ParticleSystem> ();
//		particle.Clear();
//		particle.enableEmission = true;
		target = GameObject.Find ("target");

		Vector3 pos       = target.transform.position;
		Transform particleObject        = (Transform)Instantiate(Resources.Load("prefabs/bellPangParticle", typeof(Transform)), pos, Quaternion.identity);
		particle        = (ParticleSystem)particleObject.GetComponent(typeof(ParticleSystem));
		Debug.Log ("in start");
	}
	
	// Update is called once per frame
	void Update () {
		if(isPlaying){
			isPlaying=false;
		}
	}

	public void onBellPangClicked(){
		Debug.Log ("onBellPangCllicked");
		if(!isPlaying){
			particle.transform.position = target.transform.position;

//			particle.Simulate(1.0f,true,false);
			particle.Play();
			isPlaying = true;
		}

//		particle.Emit(;
//		gameObject.SetActive (true);
	}
}
/*
ParticleSystem testParticle         = null;

// 버튼이 눌러졌을때
if (Input.GetMouseButtonUp(0) == true)
{
	// 파티클이 있고
	if (testParticle)
	{
		// 파티클이 재생중이면 재생을 멈추고 지워줍니다
		if (testParticle.isPlaying == true)
		{
			testParticle.Stop();
			testParticle.Clear();   
			
			//Debug.Log("STOP");
		}
		// 재생중이 아니라면 재생해주고요
		else
		{
			testParticle.Play();
			
			//Debug.Log("PLAY");
		}
	}
	// 파티클이 없다면 새로 만들어주구요
	else
	{
		Vector3 pos       = Vector3.zero;
		pos.y               = 3;
		pos.x               = 3 - 30.0f;
		
		Transform particleObject        = (Transform)Instantiate(Resources.Load("Prefabs/Effects/pfBlockBomb", typeof(Transform)), pos, Quaternion.identity);
		
		testParticle        = (ParticleSystem)particleObject.GetComponent(typeof(ParticleSystem));
		
		//Debug.Log("CREATE");
	}
	
	return;
}
*/