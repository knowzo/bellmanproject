﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ThrowHuddle : MonoBehaviour {

	bool isCollide=false;
	PlayerController player;
	// Use this for initialization
	void Start () {
		player = (GameObject.Find ("Runner")).GetComponent<PlayerController>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isCollide){
			if(player.isSuperMode()){
				gameObject.transform.RotateAround(gameObject.transform.position, Vector3.up, 300 * Time.deltaTime);
				gameObject.transform.RotateAround(gameObject.transform.position, Vector3.right, 300 * Time.deltaTime);
				Vector3 nextScale = gameObject.transform.localScale;
				nextScale += new Vector3(0.2f,0.2f,0.2f);
				gameObject.transform.localScale = nextScale;
				
				if(nextScale.x > 5){
					isCollide=false;
					GameObject.DestroyObject(gameObject);
				}
			}
//			else{
//				player.setPlayerCollide(true);
//			}

		}
	}

	void OnCollisionEnter2D (Collision2D other){
//		Debug.Log( "collide ??" );
		if (other.gameObject.name == "Runner") {
			isCollide=true;
			BoxCollider2D huddleBoxCollider = gameObject.GetComponent<BoxCollider2D>();
			huddleBoxCollider.isTrigger = true;
//			Debug.Log( "collide huddle" );
		}

	}
}
