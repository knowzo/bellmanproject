﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {


	public float MAX_JUMP_SPEED = 600f;

	bool collide = false;
	bool unBeatable = false;
	bool superMode = false;

	float collideTimer = 0;
	float unbeatableTimer = 0;
	float superModeTimer = 0;

	public float COLLIDE_TIME = 0.3f;
	public float UNBEATABLE_TIME = 2.0f;
	public float SUPER_TIME = 5.0f;

	HPBarController HPBar;
	float DAMAGE_CONSTANT = -0.3f;
	SpriteRenderer playerRenderer;

	int jump_level = 0;

	bool isGround;
	bool isFell;
	bool isDead;
	Camera mainCamera;
	
	// Animation Value Definition by hong
	enum PlayerState {
		RUN = 0,
		JUMP_1 = 1,
		JUMP_2 = 2
	}
	// end anim.

	// Use this for initialization
	void Start () {
		HPBar = GameObject.Find("HPBar").GetComponent<HPBarController>();
		playerRenderer = GetComponent<SpriteRenderer> ();
		isGround = true;
		isDead = false;
		mainCamera = GameObject.Find ("Main Camera").camera;
	}

	// Update is called once per frame
	void Update () 
	{
		if(!isDead){
			if (collideTimer > 0) {
				collideTimer-=Time.deltaTime;
			}

			if(gameObject.transform.position.y < -0.9f){
				isFell = true;
			}

			if (gameObject.transform.position.y < -4f) {
				HPBar.changeHP (-100f);
				isDead=true;
			}

			if (superMode) {
				superModeTimer -= Time.deltaTime;
				if (superModeTimer <= 0f) {
					superModeTimer = 0.0f;
					superMode = false;
					collide = false;
				}

			}

			if(unBeatable){
		//			collide = false;
				unbeatableTimer -= Time.deltaTime;
				if (unbeatableTimer <= 0f) {
					unbeatableTimer = 0.0f;
					unBeatable = false;
					collide = false;
					Color playerColor = playerRenderer.color;
					playerColor.a = 1.0f;
					playerRenderer.color = playerColor;
				}

			}

			if(collide){
				collideTimer-=Time.deltaTime;
				if(collideTimer<=0f){
					collideTimer=0.0f;
					collide=false;

					mainCamera.backgroundColor = new Color(1.0f,1.0f,1.0f,1.0f);
				}
			}
		}
	}
		
	void FixedUpdate()
	{

	}

	public void doJump(){
//		upSpeed = MAX_JUMP_SPEED;
		if (jump_level == 0 && isGround && !isFell) {
			rigidbody2D.AddForce (Vector2.up * MAX_JUMP_SPEED);
			jump_level++;

			// animation by hong
			Animator anim = GetComponent<Animator>();

			if(null!=anim)
				anim.SetInteger("PlayerState", (int)PlayerState.JUMP_1);
			// end anim.
		}
		else if(jump_level == 1){
			rigidbody2D.velocity = new Vector2(0.0f,0.0f);
			jump_level++;
			rigidbody2D.AddForce (Vector2.up * MAX_JUMP_SPEED);

			// animation by hong
			Animator anim = GetComponent<Animator>();
			
			if(null!=anim)
				anim.SetInteger("PlayerState", (int)PlayerState.JUMP_2);
			// end anim.
		}
//		upAccelerate = MAX_JUMP_ACCELERATE;
	}


	void OnCollisionEnter2D (Collision2D other){
		//		Debug.Log( "collide ??" );
		if (other.gameObject.tag == "huddle") {
			if(superMode){

			}
			else if(unBeatable){

			}
			else{
				collideTimer = COLLIDE_TIME;
				mainCamera.backgroundColor = new Color(1.0f,0.0f,0.0f,0.5f);
				collide=true;

				unBeatable = true;
				unbeatableTimer = UNBEATABLE_TIME;
				HPBar.changeHP (DAMAGE_CONSTANT);
				Color playerColor = playerRenderer.color;
				playerColor.a = 0.3f;
				playerRenderer.color = playerColor;
			}


			//			Debug.Log( "collide huddle" );
		}
		else if (other.gameObject.tag == "ground") 
		{
			isGround=true;
//			Debug.Log( "collide ground" );
//			if(jump_level == 2)
			jump_level=0;

			// animation by hong
			Animator anim = GetComponent<Animator> ();
			
			if (null != anim) {
				anim.SetInteger("PlayerState", (int)PlayerState.RUN);
			}
			// end anim.
		}
	}

	public bool isUnbeatable(){
		return unBeatable;
	}
	public bool isCollide(){
		return collide;
	}
	public bool isSuperMode(){
		return superMode;
	}
	public void setPlayerCollide(bool col){
		collide = col;
	}
	public void setSuperMode(){
		superMode = true;
		superModeTimer = SUPER_TIME;
	}
	public float getSuperTime(){
		return SUPER_TIME;
	}
}
