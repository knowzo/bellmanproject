﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HPBarController : MonoBehaviour {
	Image hpBarImage;
	DeathController deathController;
	bool isDead=false;
	// Use this for initialization
	void Start () {
		hpBarImage = GetComponent<Image> ();
		deathController = (GameObject.Find ("DeathController")).GetComponent<DeathController>();
	}
	
	// Update is called once per frame
	void Update () {
	//	hpBarImage.fillAmount = Mathf.Lerp (hpBarImage.fillAmount, 1f, Time.deltaTime * .5f);
		if (!isDead) {
			hpBarImage.fillAmount -= (0.02f) * Time.deltaTime;

			if (hpBarImage.fillAmount <= 0) {
					isDead = true;
					Time.timeScale = 0;
					deathController.dead ();
			}
		}
	}

	public void changeHP(float change){
		hpBarImage.fillAmount += change;
		Debug.Log ("in ChangeHP : " + change.ToString());
	}
}
