﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BellController : MonoBehaviour {
	GameObject[] bells;
	GameObject target;
	bool isRightButtonClicked = false;
	const float HEAL_CONST = 1.0f;

	const float PERFECT_CONST = 0.1f;
	const float GOOD_CONST = 0.05f;

	const float PERPECT_RATIO = 0.33f; // 0~0.33 : perfect , 0.33~0.66 : good , 0.66~1.0 : miss
	const float GOOD_RATIO = 0.9f;
	const float MISS_RATIO = 2.0f;
	int current_combo=0;

	HPBarController HPBar;
	BoosterBarController BoosterBar;

	AudioController AudioCtrl;
	ComboMessageController comboMessage;
	ComboMessageController gradeMessage;

	BellPangController bellPang;

	// Use this for initialization
	void Start () {
		target = GameObject.Find ("target");
		HPBar = GameObject.Find("HPBar").GetComponent<HPBarController>();
		BoosterBar = GameObject.Find("BoosterBar").GetComponent<BoosterBarController>();
		current_combo = 0;
		AudioCtrl = (GameObject.Find ("AudioController")).GetComponent<AudioController> ();
		comboMessage = (GameObject.Find ("ComboMessage")).GetComponent<ComboMessageController> ();
		gradeMessage = (GameObject.Find ("GradeMessage")).GetComponent<ComboMessageController> ();
		bellPang = (GameObject.Find ("BellPangController")).GetComponent<BellPangController>();
	}
	
	// Update is called once per frame
	void Update () {
		if (isRightButtonClicked) {
			bells = GameObject.FindGameObjectsWithTag ("bell");
			float min = 10000f;
			GameObject minDistanceBell = null;
			foreach (GameObject bell in bells) {
					if (bell.transform.position.x > target.transform.position.x - MISS_RATIO ) {
						float distance = Vector2.Distance(bell.transform.position,target.transform.position);
//						float distance = Mathf.Abs(bell.transform.position.x - target.transform.position.x);
						if (min > distance) {
									min = distance;
									minDistanceBell = bell;
						}
					}
			}

			Debug.Log ("Distance of Bell : " + min);
			isRightButtonClicked=false;

			if(min<=MISS_RATIO){
				float healPoint = 0;
				float boosterPoint; 

				if(min<PERPECT_RATIO){
					healPoint = PERFECT_CONST;
					boosterPoint =  PERFECT_CONST *(1.0f+current_combo/10.0f);
					current_combo++;
					AudioCtrl.playDingDong();
					gradeMessage.setComboMessage("Perfect!");
					bellPang.onBellPangClicked();
					Debug.Log ("Perfect!");
				}
				else if(min<GOOD_RATIO){
					healPoint = GOOD_CONST;
					boosterPoint =  GOOD_CONST *(1.0f+current_combo/10.0f);
					AudioCtrl.playDingDong();
					gradeMessage.setComboMessage("Good!");
					bellPang.onBellPangClicked();
					Debug.Log ("Good!");
				}
				else{
					healPoint = 0f;
					boosterPoint = 0f;
					current_combo = 0;
					gradeMessage.setComboMessage("Miss!");
					Debug.Log ("Miss!");
				}

//				Debug.Log ("HP UP : " + healPoint);
//				Debug.Log ("Booster UP : " + boosterPoint);
				if(current_combo>0){
					comboMessage.setComboMessage(current_combo.ToString() + "Combo!");
					Debug.Log ("Combo : " + current_combo);
				}
				else{
					comboMessage.setComboMessage("");
					Debug.Log ("Combo : " + current_combo);
				}

				if(healPoint>0.0f)
					HPBar.changeHP(healPoint);
				if(boosterPoint>0.0f)
					BoosterBar.changeBooster(boosterPoint);
				                     
				GameObject.DestroyObject(minDistanceBell);

			}
		}
	}

	public void clickRightButton(){
		isRightButtonClicked = true;
	}
}
