﻿using UnityEngine;
using System.Collections;

public class SettingButtonController : MonoBehaviour {
	public GameObject settingView;
	// Use this for initialization
	bool isViewing=false;
	void Start () {
		settingView.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnClickedSettingButton() {
//		Application.Quit ();

//		GameObject.
//		Instantiate (settingView);
		if(isViewing){
			settingView.SetActive (false);
			Time.timeScale = 1;
			isViewing=false;
		}
		else{
//			Debug.Log (Time.timeScale);
			settingView.SetActive (true);
			Time.timeScale = 0;
			isViewing=true;
		}
	}
}
