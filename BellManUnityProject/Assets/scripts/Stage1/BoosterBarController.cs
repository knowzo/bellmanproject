﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BoosterBarController : MonoBehaviour {
	Image boosterBarImage;
	PlayerController player;
	bool isBoosterMode=false;
	float BoosterTimer = 0f;
	float BOOSTER_TIME;

	// Use this for initialization
	void Start () {
		boosterBarImage = GetComponent<Image> ();
		player = (GameObject.Find ("Runner")).GetComponent<PlayerController>();
		BOOSTER_TIME = player.getSuperTime ();
	}
	
	// Update is called once per frame
	void Update () {
		if (isBoosterMode) {
			BoosterTimer-=Time.deltaTime;
			boosterBarImage.fillAmount -= Time.deltaTime/BOOSTER_TIME;
			if(BoosterTimer<=0){
				boosterBarImage.fillAmount = 0.0f;
				isBoosterMode=false;
				boosterBarImage.color = new Color(0.0f,0.0f,0.0f,1.0f);
			}
		}
		else{
			if (boosterBarImage.fillAmount > 0.99f) {
				player.setSuperMode();
				BoosterTimer = BOOSTER_TIME;
				isBoosterMode=true;
				boosterBarImage.color = new Color(0.0f,0.0f,1.0f,1.0f);
			}
		}
		//	hpBarImage.fillAmount = Mathf.Lerp (hpBarImage.fillAmount, 1f, Time.deltaTime * .5f);
//		boosterBarImage.fillAmount -= (0.1f) * Time.deltaTime;
	}
	
	public void changeBooster(float change){
		boosterBarImage.fillAmount += change;
//		Debug.Log ("in ChangeHP : " + change.ToString());
	}
}
