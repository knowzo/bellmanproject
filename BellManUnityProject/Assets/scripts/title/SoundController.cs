﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundController : MonoBehaviour {
	bool isClicked = false;
	Image buttonImage;
	Color buttonColor;
	void Start(){
		buttonImage = GetComponent<Image> ();
		buttonColor = buttonImage.color;
	}
	public void OnSoundButtonClicked()
	{
		if (isClicked) {
			buttonColor.a = 1.0f;
			isClicked=false;
		} else {
			buttonColor.a = 0.3f;
			isClicked=true;
		}

		buttonImage.color = buttonColor;
		Debug.Log ("test sound");
	}
}
