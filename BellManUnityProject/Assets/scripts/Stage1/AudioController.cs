﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {

	public AudioClip _playBGM;
	public AudioClip _playDingDong;

	void Awake()
	{
		DontDestroyOnLoad (gameObject);
	}
	
	public void StartGame()
	{
//		audio.clip = _playBGM;
//		audio.Play ();
	}
	
	public void playDingDong () {
		audio.PlayOneShot(_playDingDong);
	}
}
