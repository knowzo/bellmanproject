﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ComboMessageController : MonoBehaviour {
	Text comboMessage;
	// Use this for initialization
	void Start () {
		comboMessage = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void setComboMessage(string str){
		comboMessage.text = str;
	}
}
