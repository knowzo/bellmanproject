﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DeathController : MonoBehaviour {

	bool isDead=false;
	public GameObject gameoverView;

	GameOverMessageController gameoverMessageController;
	GameObject gameoverViewInstance;

	Camera mainCamera;

	void Awake(){

	}
	// Use this for initialization
	void Start () {

//		gameoverView.SetActive (false);

		mainCamera = GameObject.Find ("Main Camera").camera;
		//		gameoverView = GameObject.Find ("GameOverView");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void dead(){
		if(!isDead){

			isDead = true;
			gameoverView.SetActive (true);
			Debug.Log ("before msgCtr");
			gameoverMessageController = (GameObject.Find ("GameoverMessage")).GetComponent<GameOverMessageController>();
			if (!gameoverMessageController)
				Debug.Log ("msgCtr error");
			else
				Debug.Log ("msgCtr success");
			gameoverMessageController.viewGameoverMessage ();
//			mainCamera.backgroundColor = new Color(1.0f,1.0f,1.0f,0.7f)
		}
	}
/*	public void alive(){
		isDead = false;
	}
	*/
}
