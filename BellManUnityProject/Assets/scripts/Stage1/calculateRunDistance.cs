﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class calculateRunDistance : MonoBehaviour {

	private static float _runDistance;
	public static Text _runDistanceText;
	MapMove mapSpeedComponent;
	// Use this for initialization
	void Start () {
		_runDistance = 0;
		_runDistanceText = GetComponent<Text> ();
		mapSpeedComponent = (GameObject.Find ("MapController")).GetComponent<MapMove> ();
	}

	// Update is called once per frame
	void Update () {

		_runDistance += Time.deltaTime * (1 + mapSpeedComponent.GetMaxSpeed() / 6) * 10;
		
		int ceilingDistance = (int)Mathf.Ceil (_runDistance);
		
		_runDistanceText.text = ceilingDistance.ToString() + " m";		
	}
	
	public float GetRunDistance()
	{
		return _runDistance;
	}
	

}
